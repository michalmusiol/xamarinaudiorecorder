﻿using AudioRecorder.Droid.MediaRecorders.MediaRecordersPath;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.IO;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(AudioRecorder.Droid.MediaRecorders.VideoRecorder))]
namespace AudioRecorder.Droid.MediaRecorders
{

    public class VideoRecorder : IVideoRecorder
    {        

        public string OutputFilePatch { get; protected set; }

        public VideoRecorder()
        {
            OutputFilePatch = Path.Combine(
                Configuration.DefaultFolder,
                VideoFileNameFactory.Create());
        }

        public IVideoRecorder SetOutputFilePath(string filePath)
        {
            new VideoRecorderFilePatchValidator(filePath).Validate();
            OutputFilePatch = filePath;
            return this;
        }

        private async Task InvokeCamera()
        {
            await CrossMedia.Current.Initialize();
            var file = await CrossMedia.Current.TakeVideoAsync(new StoreVideoOptions
            {
                Directory = Configuration.TemporaryVideFolder,
                Name = Path.GetFileName(OutputFilePatch)
            });
            CopyFile(file.AlbumPath, OutputFilePatch);
        }

        private void CopyFile(string sourcePath, string destinationPath) => 
            File.Move(sourcePath, destinationPath);

        public async Task Start() => 
            await InvokeCamera();        
    }
}

