﻿using System;

namespace AudioRecorder.Droid.MediaRecorders
{
    internal static class Configuration
    {
        public static string DefaultFolder =>
            Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).AbsolutePath;

        public static string TemporaryVideoFolder =>
            "MediaRecorderTempVideos";
    }
}

