﻿namespace AudioRecorder.Droid.MediaRecorders.MediaRecordersPath
{
    internal class AudioRecorderFilePatchValidator : FilePatchValidator
    {
        public AudioRecorderFilePatchValidator(string filePath) 
            : base(filePath, AudioFileNameFactory.Extension)
        {
        }
    }
}

