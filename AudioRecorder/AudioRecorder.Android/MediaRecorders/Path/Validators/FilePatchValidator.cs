﻿using System;
using System.IO;

namespace AudioRecorder.Droid.MediaRecorders.MediaRecordersPath
{

    internal class FilePatchValidator
    {
        private readonly string _filePath;

        private readonly string _extension;

        public FilePatchValidator(string filePath, string extension)
        {
            _filePath = filePath;
            _extension = extension;
        }

        public void Validate()
        {
            ValidateFileExtension();
            ValidateFileName();
            ValidateFileDirectory();
        }
        
        private void ValidateFileExtension()
        {
            if (!_filePath.EndsWith(_extension))
            {
                throw new ArgumentException("Invalid file extension");
            }
        }

        private void ValidateFileDirectory()
        {
            if (!Directory.Exists(Path.GetDirectoryName(_filePath)))
            {
                throw new ArgumentException("Cannot access directory");
            }
        }

        private void ValidateFileName()
        {
            // TODO
        }
    }
    
}

