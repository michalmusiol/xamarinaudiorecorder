﻿namespace AudioRecorder.Droid.MediaRecorders.MediaRecordersPath
{

    internal static class FileNameFactory
    {
        public static string Create(string extension = "") =>
            $"{CreateFileName()}{extension}";

        private static string CreateFileName()
            => System.Guid.NewGuid().ToString().Replace("-", "");
    }
    
}

