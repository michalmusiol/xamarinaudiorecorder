﻿namespace AudioRecorder.Droid.MediaRecorders.MediaRecordersPath
{
    internal static class VideoFileNameFactory
    {
        public const string Extension = ".mp4";

        public static string Create() =>
            FileNameFactory.Create(Extension);
    }
    
}

