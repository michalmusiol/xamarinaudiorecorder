﻿namespace AudioRecorder.Droid.MediaRecorders.MediaRecordersPath
{
    internal static class AudioFileNameFactory
    {
        public const string Extension = ".wav";

        public static string Create() =>
            FileNameFactory.Create(Extension);
    }
    
}

