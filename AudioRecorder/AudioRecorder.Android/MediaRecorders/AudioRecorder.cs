﻿using Android.Media;
using AudioRecorder.Droid.MediaRecorders.MediaRecordersPath;
using System.IO;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(AudioRecorder.Droid.MediaRecorders.AudioRecorder))]
namespace AudioRecorder.Droid.MediaRecorders
{

    public class AudioRecorder : IAudioRecorder
    {
        protected readonly Android.Media.MediaRecorder Recorder;

        public string OutputFilePatch { get; protected set; }

        public bool IsRecording { get; protected set; }

        public AudioRecorder()
        {
            Recorder = new MediaRecorder();

            OutputFilePatch = Path.Combine(
                Configuration.DefaultFolder,
                AudioFileNameFactory.Create());
        }

        IAudioRecorder IAudioRecorder.SetOutputFilePath(string filePath)
        {
            new AudioRecorderFilePatchValidator(filePath).Validate();
            OutputFilePatch = filePath;
            return this;
        }

        private void SetUpRecorder()
        {
            Recorder.SetAudioSource(AudioSource.Mic);
            Recorder.SetOutputFormat(OutputFormat.Default);
            Recorder.SetAudioEncoder(AudioEncoder.Default);
            Recorder.SetOutputFile(OutputFilePatch);
        }

        public Task Start()
        {
            return Task.Factory.StartNew(() => {
                SetUpRecorder();
                Recorder.Prepare();
                IsRecording = true;
                Recorder.Start();
            });
        }

        public void Stop()
        {
            Recorder.Stop();
            IsRecording = false;
            Recorder.Reset();
        }
    }
}

