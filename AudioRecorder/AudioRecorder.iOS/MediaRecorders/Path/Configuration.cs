﻿using System;

namespace AudioRecorder.iOS.MediaRecorders
{
    internal static class Configuration
    {
        public static string DefaultFolder => 
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        public static string TemporaryVideoFolder =>
            "MediaRecorderTempVideos";
    }
}

