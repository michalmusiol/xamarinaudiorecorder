﻿namespace AudioRecorder.iOS.MediaRecorders.MediaRecordersPath
{
    internal class VideoRecorderFilePatchValidator: FilePatchValidator
    {

        public VideoRecorderFilePatchValidator(string filePath) 
            : base(filePath, VideoFileNameFactory.Extension)
        {
        }
    }
}

