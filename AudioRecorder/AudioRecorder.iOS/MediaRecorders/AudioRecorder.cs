﻿using AudioRecorder.iOS.MediaRecorders.MediaRecordersPath;
using AVFoundation;
using Foundation;
using System;
using System.IO;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(AudioRecorder.iOS.MediaRecorders.AudioRecorder))]
namespace AudioRecorder.iOS.MediaRecorders
{

    //Based on https://developer.xamarin.com/recipes/ios/media/sound/record_sound/
    //Not tested and not refactored
    public class AudioRecorder : IAudioRecorder
    {
        private AVAudioRecorder _recorder;
        private NSError error;
        private NSUrl url;
        private NSDictionary settings;

        public string OutputFilePatch { get; protected set; }

        public bool IsRecording { get; private set; }

        public AudioRecorder()
        {
            OutputFilePatch = Path.Combine(
                Configuration.DefaultFolder,
                AudioRecorderFilePatchValidator.Create()
                );
        }

        public IAudioRecorder SetOutputFilePath(string filePath)
        {
            new AudioRecorderFilePatchValidator(filePath).Validate();
            OutputFilePatch = filePath;

            return this;
        }

        public void Start()
        {
            var audioSession = AVAudioSession.SharedInstance();

            var err = audioSession.SetCategory(AVAudioSessionCategory.PlayAndRecord);
            if (err != null) {
                throw new Exception(err.ToString());
            }

            err = audioSession.SetActive(true);
            if (err != null)
            {
                throw new Exception(err.ToString());
            }

            url = NSUrl.FromFilename(OutputFilePatch);

            var values = new NSObject[] {
                NSNumber.FromFloat (44100.0f),
                NSNumber.FromInt32 ((int)AudioToolbox.AudioFormatType.LinearPCM),
                NSNumber.FromInt32 (2),
                NSNumber.FromInt32 (16),
                NSNumber.FromBoolean (false),
                NSNumber.FromBoolean (false)
            };

            var keys = new NSObject[] {
                AVAudioSettings.AVSampleRateKey,
                AVAudioSettings.AVFormatIDKey,
                AVAudioSettings.AVNumberOfChannelsKey,
                AVAudioSettings.AVLinearPCMBitDepthKey,
                AVAudioSettings.AVLinearPCMIsBigEndianKey,
                AVAudioSettings.AVLinearPCMIsFloatKey
            };

            NSDictionary settings = NSDictionary.FromObjectsAndKeys (values, keys);

            _recorder = AVAudioRecorder.Create(url, new AudioSettings(settings), out error);
            _recorder.PrepareToRecord();
            _recorder.Record();
            IsRecording = true;
        }

        public void Stop()
        {
            _recorder.Stop();
            IsRecording = false;
        }

        Task IMediaRecorder.Start() =>
            Task.Factory.StartNew(Start);
    }
}

