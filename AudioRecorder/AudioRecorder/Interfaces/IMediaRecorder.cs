﻿using System.Threading.Tasks;

namespace AudioRecorder
{
    public interface IMediaRecorder
    {        
        string OutputFilePatch { get; }

        Task Start();
    }    
}
