﻿namespace AudioRecorder
{
    public interface IVideoRecorder : IMediaRecorder
    {
        IVideoRecorder SetOutputFilePath(string fileName);        
    }
}
