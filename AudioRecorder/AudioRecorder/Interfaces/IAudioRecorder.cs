﻿namespace AudioRecorder
{
    public interface IAudioRecorder: IMediaRecorder
    {
        IAudioRecorder SetOutputFilePath(string fileName);

        bool IsRecording { get; }

        void Stop();
    }
}
