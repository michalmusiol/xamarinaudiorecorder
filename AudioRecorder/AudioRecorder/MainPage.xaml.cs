﻿using Android.Widget;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AudioRecorder
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        private IAudioRecorder audioRecorder => DependencyService.Get<IAudioRecorder>();

        private IVideoRecorder videoRecorder => DependencyService.Get<IVideoRecorder>();

        public MainPage() => InitializeComponent();

        #region AUDIO

        private void OnAudioStartBtnTapped(object sender, EventArgs e)
        {
            MangeAudioBtnsAvailability(true);

            audioRecorder
                .Start();
        }

        private void OnAudioStopBtnTapped(object sender, EventArgs e)
        {
            MangeAudioBtnsAvailability(false);
            audioRecorder.Stop();
            NotifyUserAboutFileCreated(audioRecorder.OutputFilePatch);
        }

        private void MangeAudioBtnsAvailability(bool isRecording)
        {
            audioStartBtn.IsEnabled = !isRecording;
            audioStopBtn.IsEnabled = isRecording;
        }

        #endregion

        #region VIDEO

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        private void OnVideoStartBtnTapped(object sender, EventArgs e) => RecordVideoAsync();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

        private async Task RecordVideoAsync()
        {
            await videoRecorder
                .Start();

            NotifyUserAboutFileCreated(videoRecorder.OutputFilePatch);
        }

        #endregion

        private void NotifyUserAboutFileCreated(string outputFilePath) =>
            Device.BeginInvokeOnMainThread(() => 
            {
                DisplayAlert("Stream recorded into:", outputFilePath, "OK");
            });
    }
}
